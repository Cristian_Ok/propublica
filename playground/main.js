////2.1
console.log("Starting javascript..");
////2.2
var myname = "Cristian";
console.log(myname);
////2.3
var ignasiAge = 32;
var age = 33;
var ageDiff = age - ignasiAge;
console.log(ageDiff);
////2.4
if (age < 21) {
   console.log("Uds, tiene menos de 21 años");
} else {
   console.log("uds, tiene 21 años o mas");
}
////2.5
if (age<ignasiAge) {
   console.log("Uds es mas joven que: ignasiAge");
} else if (age>ignasiAge) {
   console.log("Uds es mas viejo que ignasiAge ");
} else {
   console.log("Uds tiene la misma edad que ignasiAge");
}

//---------------------------- TASK 1 - PUNTO 3   ---------------------------------------------------

//3.1
// Ejercicio 1: cree una matriz con todos los nombres de su clase (incluidos los mentores).
// Ordenar la matriz alfabéticamente. Imprima el primer elemento de la matriz en la consola.
// Imprima el último elemento de la matriz en la consola. Imprima todos los elementos de la matriz en la consola.
// Use un bucle "for".
// Guarde el archivo y vuelva a cargar la página. Debería ver el primer elemento de la matriz
// ordenada, el último elemento de la matriz ordenada y una lista de todos los elementos en orden en la matriz de la consola.

var nombres = [
  "adrian",
  "carlos",
  "cristian",
  "ender",
  "mariela",
  "mike",
  "carla",
  "florencia",
  "gabriela",
  "gonzalo",
  "ornella",
  "sebastian"
];
nombres.sort();
console.log("El primer nombre ordenados es: " + nombres[0]);
console.log("El ultimo nombre ordenados es: " + nombres[nombres.length - 1]);
var i;
for (i = 0; i < nombres.length; i++) {
  console.log(nombres[i]);
}

//-------------------------------------------------------------------------------

//3.2
// Ejercicio 2: cree una matriz con todas las edades de los estudiantes de su clase. Itere la matriz con un bucle while y luego imprima todas las edades en la consola.
// Agregue un condicional dentro del ciclo while para imprimir solo números pares.
// Cambie el ciclo para usar un ciclo "for" en lugar de un ciclo "while".
// Guarde sus cambios en su archivo JavaScript.
// Vuelva a cargar la página HTML en su navegador. Debería ver cada edad impresa, luego solo los números pares impresos. Si no, investigue y solucione.

var edades = [22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35];
var i = 0;
while (i < edades.length) {
  if (edades[i] % 2 === 0) {
    console.log(edades[i]);
  }
  i++;
}
//------------------------ con For loop -------------------------------------------------------
for (let i = 0; i < edades.length; i++) {
  if (edades[i] % 2 === 0) {
    console.log(edades[i]);
  }
}
//-------------------------------------------------------------------------------
// 3.3
// Para los siguientes ejercicios, no puede ordenar su matriz. ¡Asegúrese de que su solución funcione para cualquier matriz que se haya pasado!
// Ejercicio 3: escriba una función que reciba una matriz como parámetro e imprima el número más bajo de la matriz en la consola.
// Guarde los cambios en su archivo JavaScript. Vuelva a cargar la página HTML en su navegador. Debería ver el número más bajo en la matriz impresa en la consola. Si no, investigue y solucione.

var num = [23, 55, 67, 11, 13, 45, 66, 98, 72, 9];
// function numMenor(array) {
//   var menor = array[0];
//   for (let index = 0; index < array.length; index++) {
//     if (array[index] < menor) {
//       menor = array[index];
//     }
//   }
//   return console.log(menor);
// }
// numMenor(num);
// -------------------- menor valor con Math.min-----------------------------------------
// // 3.3.a
function numMenor(array) {
  var valor = Math.min.apply(null, array);
  return console.log(valor);
}


//-------------------------------------------------------------------------------
// Ejercicio 4: escriba una función que reciba una matriz como parámetro
// e imprima el mayor número de la matriz en la consola.
// Guarde los cambios en su archivo JavaScript.
// Vuelva a cargar la página HTML en su navegador. Debería ver el número más grande en la matriz impresa en la consola.
// Si no, investigue y solucione.

// 3.4
var num = [23, 55, 67, 11, 13, 45, 66, 98, 72, 9];
function numMayor(array) {
  var valor = Math.max.apply(null, array);
  return console.log(valor);
}

//-------------------------------------------------------------------------------
// Ejercicio 5: escriba una función que reciba dos parámetros, una matriz y un índice.
// La función imprimirá el valor del elemento en la posición dada (basada en uno) en la consola.
// Por ejemplo, dada la siguiente matriz e índice, la función imprimirá '6'.
// var array = [3,6,67,6,23,11,100,8,93,0,17,24,7,1,33,45,28,33,23,12,99,100];
// var index = 1;
// Guarde los cambios en su archivo JavaScript y verifique la consola de su navegador.
//  Debería ver el número en el índice correcto impreso en la consola. Si no, investigue y solucione.

// 3.5
var array = [
  3,
  6,
  67,
  6,
  23,
  11,
  100,
  8,
  93,
  0,
  17,
  24,
  7,
  1,
  33,
  45,
  28,
  33,
  23,
  12,
  99,
  100
];
var index = 1;
function elemento(index, array) {
  return console.log(array[index]);
}

//-------------------------------------------------------------------------------
// Ejercicio 6: escriba una función que reciba una matriz y solo imprima los valores que se repiten.
// Por ejemplo, dada la siguiente matriz e índice, la función imprimirá '6,23,33,100'.
// var array = [3,6,67,6,23,11,100,8,93,0,17,24,7,1,33,45,28,33,23,12,99,100];
// Guarde los cambios en su archivo JavaScript. Vuelva a cargar la página HTML en su navegador.
//  Debería ver una matriz de los números repetidos impresos en la consola. Si no, investigue y solucione.

// 3.6
var array = [
  3,
  6,
  67,
  6,
  23,
  11,
  100,
  8,
  93,
  0,
  17,
  24,
  7,
  1,
  33,
  45,
  28,
  33,
  23,
  12,
  99,
  100
];
function repet(array) {
  array.sort();
  var num = [];
  for (let index = 1; index < array.length; index++) {
    if (array[index] === array[index - 1]) {
      num.push(array[index]);
    }
  }
  return console.log(num);
}

//-------------------------------------------------------------------------------

// 3.7
// Ejercicio 7: escriba una función JavaScript simple
//  para unir todos los elementos de la siguiente matriz en una cadena.

// myColor = ["Red", "Green", "White", "Black"];
// function cadena(array) {
//   var string = [];
//   for (let index = 0; index < array.length; index++) {
//     string += '"' + array[index] + '" ,';
//   }
//   console.log(string);
// }
// cadena(myColor);
//------------------- con metodo toString------------------------------------------------------------
myColor = ["Red", "Green", "White", "Black"];
function cadena(array) {
  var string = array.join();
  return console.log(string);
}

//-------------------------------------------------------------------------------

//----------------------- MODULO 2 TASK 1 PUNTO 4--------------------------------------------------------

//4.1
// Ejercicio 1: escriba una función de JavaScript que invierta un número.
// Por ejemplo, si x = 32443, la salida debería ser 34423.
// Guarde su archivo JavaScript y vuelva a cargar la página.
// Asegúrese de ver la salida correcta. Si no, investigue y solucione.

function inverter(num) {
  var num = num.toString();
  var x = num.length;
  var numeroInvertido = "";

  while (x >= 0) {
    numeroInvertido += num.charAt(x);
    x--;
  }
  numeroInvertido=parseInt(numeroInvertido, 10);
   return console.log(numeroInvertido);
}

//-------------------------------------------------------------------------------

//4.2

// Ejercicio 2: escriba una función de JavaScript que devuelva una cadena en orden alfabético.
// Por ejemplo, si x = 'webmaster', la salida debería ser 'abeemrstw'.
// La puntuación y los números no se pasan en la cadena.
// Guarde su archivo JavaScript y vuelva a cargar la página.
// Asegúrese de ver la salida correcta. Si no, investigue y solucione.

function sortWord(x) {
index=x.length;
x= x.split([,]);
x.sort();
var y="";
for (let index = 0; index < x.length; index++) {
    y+=x[index];
}
console.log(y);
}

//-------------------------------------------------------------------------------

//4.3
// Ejercicio 3: escriba una función de JavaScript que convierta
// la primera letra de cada palabra a mayúsculas.
//  Por ejemplo, si x = "príncipe de persia", la salida debería ser "Prince Of Persia".
// Guarde su archivo JavaScript y vuelva a cargar la página.
// Asegúrese de ver la salida correcta. Si no, investigue y solucione.

function mayString(cadena) {
  function MaysPrimera(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  var cadena3 = "";
  cadena2 = cadena.slice(0).split(" ");
  for (let i = 0; i < cadena2.length; i++) {
    cadena3 += " " + MaysPrimera(cadena2[i]) + " ";
  }
  return console.log(cadena3);
}

//-------------------------------------------------------------------------------
//4.4
// Ejercicio 4: escriba una función de JavaScript que encuentre la palabra más larga en una frase.
// Por ejemplo, si x = "Tutorial de desarrollo web", la salida debería ser "Desarrollo".
// Guarde su archivo JavaScript y vuelva a cargar la página.
// Asegúrese de ver la salida correcta. Si no, investigue y solucione.

function longWord(str) {
  function MaysPrimera(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  var cadena = str.slice(0).split(" ");
  var index = undefined;
  for (let i = 0; i < cadena.length - 1; i++) {
    if (cadena[i].length > cadena[i + 1].length) {
      index = i;
    } else {
      index = i + 1;
    }
  }
  return console.log(MaysPrimera(cadena[index]));
}
