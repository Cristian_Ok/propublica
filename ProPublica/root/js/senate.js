//------------- DECLARACION DE VARIABLES -----------------------
var checkedBoxes; // guarda la seleccion de elementos CHECKBOX seleccionados
var seleccion; // guarda la seleccion de elementos SELECT seleccionados
var members = [];
var arrayState = [];
var listState_Ordenada = [];
var miInit = {
  headers: { "X-API-Key": "i6rdxHd5cOLErYmBmwaJNH01aUCi6BIn27hGHXHY" }
};
var app = new Vue({
  el: "#app",
  data: {
    listaDeMiembros: []
  }
});
//------------------  FUNCIONES -----------------------
fetch("https://api.propublica.org/congress/v1/116/senate/members.json", miInit)
  .then(function(response) {
    return response.json();
  })
  .then(function(miJson) {
    invocacion(miJson);
  });

// onchange escucha si se des-tilda una checkbox, y activa la funcion partyStarFilter
document.getElementById("partyChekboxR").onchange = starfiltro;
document.getElementById("partyChekboxD").onchange = starfiltro;
document.getElementById("partyChekboxI").onchange = starfiltro;
document.getElementById("list-state").onchange = starfiltro;

//------------- DECLARACION DE FUNCIONES -----------------------
function invocacion(params) {
  members = params.results[0].members;
  // cargo un array con todos los item de members.state
  for (let i = 0; i < members.length; i++) {
    arrayState.push(members[i].state);
  }
  // ordeno la lista quitando los valores repetidos
  listState_Ordenada = arrayState.filter(
    (valor, indiceActual, arreglo) => arreglo.indexOf(valor) === indiceActual
  );
  //crear la select list en el HTML
  select_list_state(listState_Ordenada);
  // genera funcion para filtro final, se imprime la tabla con los filtros chekbox y la lista
  starfiltro();
}
//genero funcion para crear la select list en el HTML
function select_list_state(array) {
  var lista = "";
  for (let i = 0; i < array.length; i++) {
    lista +=
      '<option value="' + array[i] + '">' + "" + array[i] + "" + "</option>";
  }
  lista += '<option value="All-State" selected >All States</option>';
  return (document.getElementById("list-state").innerHTML = lista);
}
// chequea si los valores seleccionados existen en el member.party y en member.select
// para return true, invoca a filter() y lo guarda en un nuevo array, para false, salta al elemento que sigue
function filtrado(member) {
  if (
    checkedBoxes.includes(member.party) &&
    (member.state == seleccion || seleccion == "All-State")
  ) {
    return true;
  }
}
// se creo para ser invocada por onchange en cuanto se produzca un cambio.
//Dentro de esta llama a tableMembers() y construye la tabla filtrada
function starfiltro() {
  checkedBoxes = Array.from(
    document.querySelectorAll("input[name=mycheckboxes]:checked")
  ).map(elt => elt.value);
  seleccion = document.getElementById("list-state").value;
  var miembros_filtrados = members.filter(filtrado);
  app.listaDeMiembros = miembros_filtrados;
}
