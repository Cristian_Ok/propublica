//----------------   CONDICION PARA MI SCRIPT DINAMICO    -------------------------
if (document.getElementById("senatte")) {
  var miInit = {
    headers: { "X-API-Key": "i6rdxHd5cOLErYmBmwaJNH01aUCi6BIn27hGHXHY" }
  };
  fetch(
    "https://api.propublica.org/congress/v1/116/senate/members.json",
    miInit
  )
    .then(function(response) {
      return response.json();
    })
    .then(function(miJson) {
      var data = miJson;
      statisticsData(data);
    });
} else {
  var miInit = {
    headers: { "X-API-Key": "i6rdxHd5cOLErYmBmwaJNH01aUCi6BIn27hGHXHY" }
  };
  fetch("https://api.propublica.org/congress/v1/116/house/members.json", miInit)
    .then(function(response) {
      return response.json();
    })
    .then(function(miJson) {
      var data = miJson;
      statisticsData(data);
    });
}
//----------------   MI OBJETO VUE     -------------------------
var app = new Vue({
  el: "#app",
  data: {
    statistics: {
      nro_of_democrats: 0,
      nro_of_republicans: 0,
      nro_of_independents: 0,
      nro_rep_total: 0,
      porc_voted_with_partyD: 0,
      porc_voted_with_partyR: 0,
      porc_voted_with_partyI: 0,
      porc_voted_with_partyT: 0,
      least_engaged: [],
      most_engaged: [],
      least_loyal: [],
      most_loyal: []
    }
  }
});
//----------------       DECLARACION DE FUNCIONES     -------------------------
function statisticsData(data) {
  // FRACCIONO EN 3 ARRAYS EL OBJETO MIEMBROS EN FUNCION DE LA PROPIEDAD PARTY
    // VAR DECLARATION
    var miembros = data.results[0].members;
    var miembrosD = [];
    var miembrosR = [];
    var miembrosI = [];
    var tablaParty = "";
  for (let i = 0; i < miembros.length; i++) {
    if (miembros[i].party === "D") {
      miembrosD.push(miembros[i]);
    } else if (miembros[i].party === "R") {
      miembrosR.push(miembros[i]);
    } else {
      miembrosI.push(miembros[i]);
    }
  }
  app.statistics.nro_of_democrats = miembrosD.length;
  app.statistics.nro_of_republicans = miembrosR.length;
  app.statistics.nro_of_independents = miembrosI.length;
  app.statistics.nro_rep_total = miembrosD.length + miembrosR.length + miembrosI.length;
  app.statistics.porc_voted_with_partyD = parseFloat(
    pctVotes(miembrosD)
  );
  app.statistics.porc_voted_with_partyR = parseFloat(
    pctVotes(miembrosR)
  );
  app.statistics.porc_voted_with_partyI = parseFloat(
    pctVotes(miembrosI)
  );
  app.statistics.porc_voted_with_partyT = 100;
  app.statistics.most_engaged = mostEngaged(miembros);
  app.statistics.least_engaged = leastEngaged(miembros);
  app.statistics.most_loyal = mostLoyal(miembros);
  app.statistics.least_loyal = leastLoyal(miembros);
}

// DECLARACION DE FUNCIONES
// PORCENTAJE DE MIEMBORS X PARTIDO
function pctVotes(array) {
  var mediaVotes = 0;
  for (let i = 0; i < array.length; i++) {
    mediaVotes += array[i].votes_with_party_pct;
  }
  mediaVotes = mediaVotes / array.length;
  return mediaVotes.toFixed(2);
}

// 10%MOST ENGAGED
function mostEngaged(arrayArg) {
  var array = arrayArg.slice();
  array.sort((a, b) => a.missed_votes_pct - b.missed_votes_pct);
  var corte = Math.ceil((array.length * 10) / 100) - 1;
  var newarray = [];
  for (let i = corte; i < array.length; i++) {
    if (array[i].missed_votes_pct === array[i + 1].missed_votes_pct) {
      corte++;
    } else {
      break;
    }
  }
  newarray = array.slice(0, corte + 1);
  return newarray;
}
// 10% LAST ENGAGED
function leastEngaged(arrayArg) {
  var array = arrayArg.slice();
  array.sort((a, b) => b.missed_votes_pct - a.missed_votes_pct);
  var corte = Math.ceil((array.length * 10) / 100) - 1;
  var newarray = [];
  for (let i = corte; i < array.length; i++) {
    if (array[i].missed_votes_pct === array[i + 1].missed_votes_pct) {
      corte++;
    } else {
      break;
    }
  }
  newarray = array.slice(0, corte + 1);
  return newarray;
}
// 10 % LAST LOYALITY
function leastLoyal(arrayArg) {
  var array = arrayArg.slice();
  array.sort((a, b) => a.votes_with_party_pct - b.votes_with_party_pct);
  var corte = Math.ceil((array.length * 10) / 100) - 1;
  var newarray = [];
  for (let i = corte; i < array.length; i++) {
    if (array[i].votes_with_party_pct === array[i + 1].votes_with_party_pct) {
      corte++;
    } else {
      break;
    }
  }
  newarray = array.slice(0, corte + 1);
  return newarray;
}
// 10 % MOST LOYALITY
function mostLoyal(arrayArg) {
  var array = arrayArg.slice();
  array.sort((a, b) => b.votes_with_party_pct - a.votes_with_party_pct);
  var corte = Math.ceil((array.length * 10) / 100) - 1;
  var newarray = [];
  for (let i = corte; i < array.length; i++) {
    if (array[i].votes_with_party_pct === array[i + 1].votes_with_party_pct) {
      corte++;
    } else {
      break;
    }
  }
  newarray = array.slice(0, corte + 1);
  return newarray;
}
